import datetime
from django.shortcuts import render
from testimoni.forms import testimoni
from testimoni.models import Testimoni

def testimoni_page(request):
    form = testimoni()
    date = datetime.datetime.now()
    response = {'form': form, 'allComment': Testimoni.objects.all().order_by('-time'), 'datetime': date}
    return render(request, 'testimoni.html', response)


response = {}
def add_testimoni(request):
    if request.method == "POST":
        form = testimoni(request.POST or None)
        if form.is_valid():
            response['name'] = request.user.first_name + " " + request.user.last_name 
            response['comment'] = request.POST['comment']
            testi = Testimoni(comment=response['comment'], name = response['name'])
            testi.save()
            return render(request, 'testimoni.html', response)
        
