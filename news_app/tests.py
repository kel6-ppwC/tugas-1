from django.test import TestCase, Client
from django.urls import resolve
from .views import news
class Lab3Test(TestCase):
    def test_lab_3_url_is_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code,200)
    def test_lab_3_using_to_do_list_template(self):
        response = Client().get('/news/')
        self.assertTemplateUsed(response, 'news.html')
    def test_lab_3_using_index_func(self):
        found = resolve('/news/')
        self.assertEqual(found.func, news)
# Create your tests here.
