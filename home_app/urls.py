from . import views
from django.urls import path, include

urlpatterns = [
    path('', views.home_app, name='index'),
    path('halaman_utama', views.halaman_utama, name='utama'),
]
