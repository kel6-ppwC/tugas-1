from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import event, join_event, participate, attended_event
from .models import Event, Participant
from .forms import ParticipantForm
from django.utils import timezone
# Create your tests here.

class EventTest(TestCase):
    def test_is_url_exist(self):
        response = Client().get('/events/')
        self.assertEqual(response.status_code, 200)
## GDE NAMBAHIN INI DIBAWAH
    def test_is_url_exist_join_event(self):
        response = Client().get('/events/join-event/')
        self.assertEqual(response.status_code, 302)

    def test_is_url_exist_participate(self):
        response = Client().get('/events/join-participate/')
        self.assertEqual(response.status_code, 404)

    def test_is_url_exist_attended_event(self):
        response = Client().get('/events/attended-event/')
        self.assertEqual(response.status_code, 200)
## GDE NAMBAHIN DIATAS
    def test_event_page_using_event_func(self):
        found = resolve('/events/')
        self.assertEqual(found.func, event)
            
    def test_event_page_using_join_event_func(self):
        found = resolve('/events/join-event/')
        self.assertEqual(found.func, join_event)
            
    def test_event_page_using_participate_func(self):
        found = resolve('/events/participate/')
        self.assertEqual(found.func, participate)
## GDE NAMBAHIN INI DIBAWAH
    def test_event_page_attended_event(self):
        found = resolve('/events/attended-event/')
        self.assertEqual(found.func, attended_event)
## GDE NAMBAHIN DIATAS    
    def test_template_event_is_used(self):
        response = Client().get('/events/')
        self.assertTemplateUsed(response, 'event.html')
		
    # def test_model_can_create_new_event(self):
    # #Creating a new Event
    #         new_event = Event.objects.create(nama_event='Nice', tanggal=timezone.now, deskripsi = 'seru', foto_event = 'foto.jpg')

    # #Retrieving all available activity
    #         counting_event = Event.objects.all().count()
    #         self.assertEqual(counting_event,1)
            
    def test_model_can_create_new_participant(self):
    #Creating a new Event
            new_participant = Participant.objects.create(nama='Adi', username='adisryn', email = 'a@gmail.com', event = 'cgt')

    #Retrieving all available activity
            counting_participant = Participant.objects.all().count()
            self.assertEqual(counting_participant,1)
            
    def test_form_input_has_placeholder_and_css_classes(self):
            form = ParticipantForm()
            self.assertIn('class="form-control', form.as_p())



