from django.shortcuts import render
from event_app.models import Event
from news_app.models import News
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
# Create your views here.


def home_app(request):
    return render(request, 'index.html')

def halaman_utama(request):
    response = {'object_news' : News.objects.all()[:2], 'object_event' : Event.objects.all()[:3]}
    return render(request, 'utama.html',response)

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home_app:index')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})