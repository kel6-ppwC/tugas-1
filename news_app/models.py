from django.db import models
from datetime import datetime

# Create your models here.
class News(models.Model):
    date = models.DateTimeField()
    newsTitle = models.TextField()
    newsDetail = models.TextField()
    model_pic = models.TextField()

def __str__(self):
    return self.newsTitle

# Create your models here.
