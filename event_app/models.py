from django.db import models

# Create your models here.
class Event(models.Model):
    nama_event = models.CharField(max_length=100)
    tanggal = models.DateField()
    deskripsi = models.TextField()
    foto_event = models.TextField()


    def __str__(self):
        return self.nama_event



class Participant(models.Model):
    nama = models.CharField(max_length = 100)
    username = models.CharField(max_length=50)
    email = models.EmailField()
    event = models.CharField(max_length=100)

    def __str__(self):
        return self.nama
