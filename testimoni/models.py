from django.db import models
# Create your models here.
from django.utils import timezone

class Testimoni(models.Model):
    name = models.CharField(max_length=300)
    comment = models.CharField(max_length=300)
    time = models.DateTimeField(default=timezone.now)

    def __str__(self):
    	return self.name