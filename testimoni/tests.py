from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import testimoni_page,add_testimoni
from .models import Testimoni
from .forms import testimoni
from django.utils import timezone

class TetimoniUnitTest(TestCase):
	def test_testimoni_url_exist(self):
		response = Client().get('/testimoni/')
		self.assertEqual(response.status_code,200)

	def test_testimoni_url_not_exist(self):
		response = Client().get('/testimoni/')
		self.assertNotEqual(response.status_code,404)
		
	def test_using_testimoni_page(self):
		found = resolve('/testimoni/')
		self.assertEqual(found.func ,testimoni_page)
		
	def test_template_testimoni_is_used(self):
		response = Client().get('/testimoni/')
		self.assertTemplateUsed(response, 'testimoni.html')
		
	# def test_model_can_create_new_testimoni(self):
 #        #Creating a new Status
	# 	new_status = Testimoni.objects.create(comment='Nice', time=timezone.now)

 #        #Retrieving all available activity
	# 	counting_testimoni = Testimoni.objects.all().count()
	# 	self.assertEqual(counting_testimoni,1)
		
	def test_form_testimoni_input_has_placeholder_and_css_classes(self):
		form = testimoni()
		self.assertIn('class="form-control', form.as_p())