from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.testimoni_page),
    path('addtestimoni/', views.add_testimoni),
]
