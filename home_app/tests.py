from django.test import TestCase
from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views
# Create your tests here.

class HomeUnitTest(TestCase):
    def test_url_home_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_url_utama_exist(self):
        response = Client().get('/halaman_utama')
        self.assertEqual(response.status_code, 200)

    def test_using_home_app_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.home_app)

    def test_using_index_func(self):
        found = resolve('/halaman_utama')
        self.assertEqual(found.func, views.halaman_utama)
