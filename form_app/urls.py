from django.conf.urls import url
from django.urls import re_path
from .views import index,add_member

urlpatterns = [
    re_path('registration/', index, name='registration'),
	re_path('add_member/', add_member, name = 'add_member')
]
