from django.shortcuts import render
from .models import Event, Participant
from .forms import ParticipantForm
from django.http import HttpResponseRedirect
from . import forms
import json
# Create your views here.

response ={}
def event(request):
    event = Event.objects.all()
    participant = Participant.objects.all()
    response['all_event'] = event
    response['participant'] = participant
    html = 'event.html'
    return render(request, html, response)

def join_event(request):
	if not request.user.is_authenticated :
		return HttpResponseRedirect('/')
	response['join_event'] = ParticipantForm
	html = 'join-event.html'
	return render(request, html, response)
	
def attended_event(request):
	list_event = []
	list_user = Participant.objects.all()
	list_daftar_event = Event.objects.all()
	if request.user.is_authenticated:
		for i in list_user:
			if i.email == request.user.email:
				list_event.append(i)
	context = {
		'list_event' : list_event,
		'list_daftar_event' : list_daftar_event,
		'banyak_event' : len(list_event)
	}
	response = render(request, 'attended.html', context = context)
	return response


def participate(request):
	form = forms.ParticipantForm(request.POST)
	# objek_member = Event.objects.values_list('username')
	if (form.is_valid()):
		respond = HttpResponseRedirect('/events/')
		response['nama'] = request.POST['nama']
		response['username'] = request.POST['username']
		response['email'] = request.user.email
		response['event'] = form.cleaned_data['event']
		member = Participant(nama = response['nama'], username = response['username'], email = response['email'], event = response['event'])
		member.save()
		return respond
	else:
		form = forms.ParticipantForm()

	return render(request, 'event.html', response)
